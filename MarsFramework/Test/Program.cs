﻿using NUnit.Framework;
using MarsFramework.Pages;

namespace MarsFramework
{
    public class Program
    {
        [TestFixture]
        [Category("Sprint1")]
        class User : Global.Base
        {

            [Test]
            public void ShareSkill()
            {
                extent.StartTest("Enter and Save ShareSkill");
                ShareSkill shareskillobj = new ShareSkill();
                shareskillobj.EnterShareSkill();

            }

            [Test]
            public void ViewListings()
            {
                extent.StartTest("View Manage Listings");
                ManageListings manageListingobj = new ManageListings();
                manageListingobj.viewListings();
            }

            [Test]
            public void DeleteListings()
            {
                extent.StartTest("Delete one of Listings");
                ManageListings manageListingobj = new ManageListings();
                manageListingobj.DeleteListings();
            }

            [Test]
            public void EditListings()
            {
                extent.StartTest("Edit one of the Share Skill from Listings");
                ManageListings manageListingobj = new ManageListings();
                manageListingobj.EditList();

            }


        }
    }
}