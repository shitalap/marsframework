﻿using MarsFramework.Global;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Threading;

namespace MarsFramework.Pages
{
    internal class ManageListings
    {
        public ManageListings()
        {
            PageFactory.InitElements(Global.GlobalDefinitions.driver, this);
        }

        //Click on Manage Listings Link
        [FindsBy(How = How.LinkText, Using = "Manage Listings")]
        private IWebElement manageListingsLink { get; set; }

        //View the listing
        [FindsBy(How = How.XPath, Using = "(//i[@class='eye icon'])[1]")]
        private IWebElement view { get; set; }

        //Delete the listing
       // [FindsBy(How = How.XPath, Using = "//table[1]/tbody[1]")]
        [FindsBy(How = How.XPath, Using = "//tr[1]//td[8]//div[1]//button[3]//i[1]")]
        private IWebElement delete { get; set; }

        //Edit the listing
       // [FindsBy(How = How.XPath, Using = "(//i[@class='outline write icon'])[1]")]
        [FindsBy(How = How.XPath, Using = "//tr[1]//td[8]//div[1]//button[2]")]
        private IWebElement edit { get; set; }

        //Click on Yes or No
        // [FindsBy(How = How.XPath, Using = "//div[@class='actions']")]
        // [FindsBy(How = How.XPath, Using = "//button[@class='ui icon positive right labeled button']")]
        [FindsBy(How = How.XPath, Using = "//button[@class='ui negative button']")]
        private IWebElement clickActionsButton { get; set; }

        internal void Listings()
        {
            //Populate the Excel Sheet
            GlobalDefinitions.ExcelLib.PopulateInCollection(Base.ExcelPath, "ManageListings");
            
            //Click on Manage Listing Link
            Thread.Sleep(500);
            manageListingsLink.Click();
            
        }

        internal void viewListings()
        {
            //Click on Manage Listing Link
            Thread.Sleep(500);
            manageListingsLink.Click();

            //Click on View Listing option
            Thread.Sleep(500);
            view.Click();

        }

        internal void EditList()
        {
            //Populate the Excel Sheet
            GlobalDefinitions.ExcelLib.PopulateInCollection(Base.ExcelPath, "ManageListings");
           
            //Click on Manage Listing Link
            Thread.Sleep(1000);
            manageListingsLink.Click();
            Thread.Sleep(500);

            //Click on Edit List icon
            edit.Click();
            Thread.Sleep(500);

            //Edit Share Skill Page 
            ShareSkill shareskillobj = new ShareSkill();
            shareskillobj.EditShareSkill();

        }

        internal void DeleteListings()
        {
            //Populate the Excel Sheet
            GlobalDefinitions.ExcelLib.PopulateInCollection(Base.ExcelPath, "ManageListings");

            //Click on Manage Listing Link
            Thread.Sleep(1000);
            manageListingsLink.Click();
            Thread.Sleep(500);

            //Click on Delete Listiong option
            Thread.Sleep(1000);
            delete.Click();

            clickActionsButton.Click();
            Thread.Sleep(1000);

           /* IWebElement element = GlobalDefinitions.driver.FindElement(By.XPath("//div[@class='header']"));

            // 'IJavaScriptExecutor' is an interface which is used to run the 'JavaScript code' into the webdriver (Browser)
            ((IJavaScriptExecutor)GlobalDefinitions.driver).ExecuteScript("arguments[0].click()", element);
            element.Click();

            // Switch the control of 'driver' to the Alert from main window
            IAlert confirmationAlert = GlobalDefinitions.driver.SwitchTo().Alert();

            // Get the Text of Alert
            String alertText = confirmationAlert.Text;

            Console.WriteLine("Alert text is " + alertText);

            //'.Dismiss()' is used to cancel the alert '(click on the Cancel button)'
            confirmationAlert.Dismiss();
            */

        }
    }
}
