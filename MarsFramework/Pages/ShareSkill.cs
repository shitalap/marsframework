﻿using MarsFramework.Global;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace MarsFramework.Pages
{
    internal class ShareSkill
    {
        private object document;

        public ShareSkill()
        {
            PageFactory.InitElements(GlobalDefinitions.driver, this);
        }

        //Click on ShareSkill Button
        [FindsBy(How = How.LinkText, Using = "Share Skill")]
        private IWebElement ShareSkillButton { get; set; }

        //Enter the Title in textbox
        [FindsBy(How = How.Name, Using = "title")]
        private IWebElement Title { get; set; }

        //Enter the Description in textbox
        [FindsBy(How = How.Name, Using = "description")]
        private IWebElement Description { get; set; }

        //Click on Category Dropdown
        [FindsBy(How = How.Name, Using = "categoryId")]
        private IWebElement CategoryDropDown { get; set; }

        //Click on SubCategory Dropdown
        [FindsBy(How = How.Name, Using = "subcategoryId")]
        private IWebElement SubCategoryDropDown { get; set; }

        //Enter Tag names in textbox
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[@id='service-listing-section']/div[contains(@class,'ui container')]/div[contains(@class,'listing')]/form[contains(@class,'ui form')]/div[contains(@class,'tooltip-target ui grid')]/div[contains(@class,'twelve wide column')]/div[contains(@class,'')]/div[contains(@class,'ReactTags__tags')]/div[contains(@class,'ReactTags__selected')]/div[contains(@class,'ReactTags__tagInput')]/input[1]")]
        private IWebElement Tags { get; set; }

       // IWebElement Servicetypeelement = GlobalDefinitions.driver.FindElement(By.Name("serviceType"));

        //Select the Service type
       [FindsBy(How = How.XPath, Using = "//form/div[5]/div[@class='twelve wide column']/div/div[@class='field']")]
       private IWebElement ServiceTypeOptions { get; set; }

        //Select the Location Type
        [FindsBy(How = How.XPath, Using = "//form/div[6]/div[@class='twelve wide column']/div/div[@class = 'field']")]
        private IWebElement LocationTypeOption { get; set; }

        //Click on Start Date dropdown
        [FindsBy(How = How.Name, Using = "startDate")]
        private IWebElement StartDateDropDown { get; set; }

        //Click on End Date dropdown
        [FindsBy(How = How.Name, Using = "endDate")]
        private IWebElement EndDateDropDown { get; set; }

        //Storing the table of available days
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[@id='service-listing-section']/div[@class='ui container']/div[@class='listing']/form[@class='ui form']/div[7]/div[2]/div[1]")]
        private IWebElement Days { get; set; }

        //Storing the starttime
        [FindsBy(How = How.XPath, Using = "//div[3]/div[2]/input[1]")]
        private IWebElement StartTime { get; set; }

        //Click on StartTime dropdown
        [FindsBy(How = How.XPath, Using = "//div[3]/div[2]/input[1]")]
        private IWebElement StartTimeDropDown { get; set; }

        //Click on EndTime dropdown
        [FindsBy(How = How.XPath, Using = "//div[3]/div[3]/input[1]")]
        private IWebElement EndTimeDropDown { get; set; }

        //Click on Skill Trade option
        [FindsBy(How = How.XPath, Using = "//form/div[8]/div[@class='twelve wide column']/div/div[@class = 'field']")]
        private IWebElement SkillTradeOption { get; set; }

        //Enter Skill Exchange
        [FindsBy(How = How.XPath, Using = "//div[@class='form-wrapper']//input[@placeholder='Add new tag']")]
        private IWebElement SkillExchange { get; set; }

        //Enter the amount for Credit
        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Amount']")]
        private IWebElement CreditAmount { get; set; }

        //Click on Active/Hidden option
        [FindsBy(How = How.XPath, Using = "//form/div[10]/div[@class='twelve wide column']/div/div[@class = 'field']")]
        private IWebElement ActiveOption { get; set; }

        //Click on Save button
        [FindsBy(How = How.XPath, Using = "//input[@value='Save']")]
        private IWebElement Save { get; set; }

        internal void EnterShareSkill()
        {
            //Populating data from Excel
            GlobalDefinitions.ExcelLib.PopulateInCollection(Base.ExcelPath, "ShareSkill");

            //Click on Share Skill button
            ShareSkillButton.Click();
            Thread.Sleep(500);

            //Enter Title
            Title.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "Title"));

            //Enter Description
            Description.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "Description"));
            Thread.Sleep(500);

            //Select Category
            CategoryDropDown.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "Category"));
            Thread.Sleep(500);

            //Select SubCategory
            SubCategoryDropDown.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "SubCategory"));
            Thread.Sleep(500);


            //Enter Tags
            Tags.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "Tags"));

            //Scroll down the page
            ((IJavaScriptExecutor)GlobalDefinitions.driver).ExecuteScript("window.scrollTo(0, 300)");

           IWebElement Servicetypeelement = GlobalDefinitions.driver.FindElement(By.Name("serviceType"));
            
            Actions actions = new Actions(GlobalDefinitions.driver);
            actions.MoveToElement(Servicetypeelement);
            actions.Build().Perform();
            actions.Click();

            //Select Service Type Radio button
            //ServiceTypeOptions.Click();

            // Store all the elements of same category in the list of WebLements 
            IList<IWebElement> ServiceTypeRadioBttn = GlobalDefinitions.driver.FindElements(By.Name("serviceType"));

            // Create a boolean variable which will hold the value (True/False)
            bool bValue = false;
            //this statement will return True, in case of first Radio button is selected
            bValue = ServiceTypeRadioBttn.ElementAt(0).Selected;

            // This will check that if the bValue is True means if the first radio button is selected
            if (bValue == true)
            {
                // This will select Second radio button, if the first radio button is selected by default
                ServiceTypeRadioBttn.ElementAt(1).Click();
            }
            else
            {
                // If the first radio button is not selected by default, the first will be selected
                ServiceTypeRadioBttn.ElementAt(0).Click();
            }


            //Select Location Type Radio button
            //  LocationTypeOption.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "LocationType"));
            // Store all the elements of same category in the list of WebLements 
            IList<IWebElement> LocationTypeRadioBttn = GlobalDefinitions.driver.FindElements(By.Name("locationType"));

            // Create a boolean variable which will hold the value (True/False)
            bool bValue1 = false;
            //this statement will return True, in case of first Radio button is selected
            bValue = LocationTypeRadioBttn.ElementAt(0).Selected;

            // This will check that if the bValue is True means if the first radio button is selected
            if (bValue1 == true)
            {
                // This will select Second radio button, if the first radio button is selected by default
                LocationTypeRadioBttn.ElementAt(1).Click();
            }
            else
            {
                // If the first radio button is not selected by default, the first will be selected
                LocationTypeRadioBttn.ElementAt(0).Click();
            }

            //Scroll down the page
            ((IJavaScriptExecutor)GlobalDefinitions.driver).ExecuteScript("window.scrollTo(0, 300)");

            //Enter Skill Exchange
            SkillExchange.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "Skill-Exchange"));

            //Click on Save button
            Save.Click();


        }

        internal void EditShareSkill()
        {
            //Populating data from Excel
            GlobalDefinitions.ExcelLib.PopulateInCollection(Base.ExcelPath, "EditShareSkill");

            //Clear Title text box
            Title.Clear();
            
            //Enter  Title to Edit
            Title.SendKeys(GlobalDefinitions.ExcelLib.ReadData(2, "Title"));
            Thread.Sleep(500);

            //Scroll down the page
            ((IJavaScriptExecutor)GlobalDefinitions.driver).ExecuteScript("window.scrollTo(0, 500)");

            //Click on Save button
            Save.Click();

        }
    }
}
